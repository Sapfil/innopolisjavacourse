package ru.innopolis.uni.course2.deadLockEmulation;

/**
 * by Andrey Kostrov on 07.11.2016.
 */
public class Main {



    public static void main(String[] args) {
        Object monitor1 = new Object();
        Object monitor2 = new Object();

        DeadThread deadThread1 = new DeadThread(monitor1, monitor2);
        DeadThread deadThread2 = new DeadThread(monitor1, monitor2);

        deadThread1.start();
        deadThread2.start();

        System.out.println("Main thread is end");
    }
}
