package ru.innopolis.uni.course2.deadLockEmulation;

/**
 * by Andrey Kostrov on 07.11.2016.
 */
public class DeadThread extends Thread{

    private Object monitor1, monitor2;

    DeadThread(Object monitor1, Object monitor2) {
        this.monitor1 = monitor1;
        this.monitor2 = monitor2;
    }

    @Override
    public void run(){
        while(!isInterrupted()) {

            System.out.println("Thread " + this.getName() + "entering 'synchronized (monitor1)' block");
            synchronized (monitor1) {
                System.out.println("Thread " + this.getName() + "is in 'synchronized (monitor1)' block");
                synchronized (monitor2){
                    System.out.println("Thread " + this.getName() + "is in inner 'synchronized (monitor2)' block");
                }
            }

            System.out.println("Thread " + this.getName() + "entering 'synchronized (monitor2)' block");
            synchronized (monitor2) {
                System.out.println("Thread " + this.getName() + "is in 'synchronized (monitor2)' block");
                synchronized (monitor1){
                    System.out.println("Thread " + this.getName() + "is in inner 'synchronized (monitor1)' block");
                }
            }
        }
    }
}
