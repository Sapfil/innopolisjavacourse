package ru.innopolis.uni.course2.littleHomeWork2_XML;

import java.io.BufferedReader;
import java.io.FileWriter;
import java.io.IOException;
import java.lang.reflect.Field;

/**
 * Machine for serialization objects to XML-style
 * and for deserialization objects from XML-file
 * Constructor is private.
 * @author  Andrey Kostrov on 09.11.2016.
 */
public class Serializer_Deserializer {

    private  Serializer_Deserializer(){};

    /**
     * Serializing fields of object and writes it to XML
     * private fields will bw serialized too
     * @param f input object
     * @param objectName object name (field name in super-object)
     * @param writer BufferedWriter wrapped output XML-file
     * @param tab tabulation for correct writing to XML (tab for sub-object is "tab + \t"
     * @throws IOException if something goes wrong with file writing
     */
    static void serialization(Object f, String objectName, FileWriter writer, String tab) throws IOException {

        Class classname = f.getClass();

        Field[] fields = classname.getDeclaredFields();
        writer.write("\n" + tab + "<" + objectName + " type=\"" + classname.getName() + "\">");

        for(Field field : fields) {
            field.setAccessible(true);
            String value = null;
            try {
                switch(field.getType().getSimpleName()){
                    case "boolean": {value = Boolean.toString(field.getBoolean(f)); break; }
                    case "byte":    {value = Byte.toString(field.getByte(f)); break; }
                    case "char":    {value = Character.toString(field.getChar(f)); break; }
                    case "double":  {value = Double.toString(field.getDouble(f)); break; }
                    case "float":   {value = Double.toString(field.getFloat(f)); break; }
                    case "int":     {value = Integer.toString(field.getInt(f)); break; }
                    case "long":    {value = Long.toString(field.getLong(f)); break; }
                    case "String":  {value = field.get(f).toString(); break;}
                    default: {
                        //TODO arrays and collections serialization)
                        serialization(field.get(f), field.getName(), writer, tab + "\t");
                    }
                }} catch (IllegalAccessException e) {
                e.printStackTrace();
            }

            if (value != null)
                writer.write("\n" + tab + "\t<" + field.getName() + " type=\"" + field.getType().getSimpleName() + "\" value=\"" + value + "\" />");
        }
        writer.write("\n" + tab + "</" + objectName + ">");
    }

    /**
     * Crates stub-object and then fills his fields with values, got from XML
     * @param reader BufferedReader wrapped XML file
     * @param objectClassName Class of stub-object
     * @return object that can be normally cast to "objectClassName"
     * @throws IOException if something goes wrong while reading XML
     * @throws ClassNotFoundException if there is error in XML in class full name
     * @throws IllegalAccessException if there is error in XML in class full name
     * @throws InstantiationException if there is no opportunity to create new instance of class
     *                  (maybe there is no public constructor or there is no default constructor)
     */
    static Object deserialization(BufferedReader reader, String objectClassName) throws IOException, ClassNotFoundException, InstantiationException, IllegalAccessException {

        Object deserializeObject = Class.forName(objectClassName).newInstance();
        Field[] fields = deserializeObject.getClass().getDeclaredFields();
        for (Field field1 : fields) {
            field1.setAccessible(true);
        }

        int startBlockIndex;
        String[] deserializeField = new String[3];
        String newString = reader.readLine();

        // ******* reading string
        // ******* deserializeField[0] is name of filed, fond in string
        // ******* deserializeField[1] is type of filed
        // ******* deserializeField[2] is value of filed (only for primitive types)
        while (!newString.startsWith("</")){
            char[] stringToChar = newString.toCharArray();
            int i = 0;
            while (stringToChar[i] != '<') i++;
            startBlockIndex = i + 1;
            while (stringToChar[i] != ' ') i++;
            deserializeField[0] = new String(stringToChar, startBlockIndex, i - startBlockIndex);
            while (stringToChar[i] != '"') i++;
            startBlockIndex = i + 1;
            i++;
            while (stringToChar[i] != '"') i++;
            deserializeField[1] = new String(stringToChar, startBlockIndex, i - startBlockIndex);
            i++;
            if (newString.contains("value")) { // ***** only for primitive types
                while (stringToChar[i] != '"' && i < stringToChar.length) i++;
                startBlockIndex = i + 1;
                i++;
                while (stringToChar[i] != '"' && i < stringToChar.length) i++;
                deserializeField[2] = new String(stringToChar, startBlockIndex, i - startBlockIndex);
            }

            for (Field field : fields) {
                if (field.getName().equals(deserializeField[0]))
                    switch (deserializeField[1]) {
                        case "boolean": {field.setBoolean(deserializeObject,Boolean.parseBoolean(deserializeField[2])); break; }
                        case "byte":    {field.setByte(deserializeObject,   Byte.parseByte(deserializeField[2])); break; }
                        case "char":    {field.setChar(deserializeObject,   deserializeField[2].charAt(0)); break; }
                        case "double":  {field.setDouble(deserializeObject, Double.parseDouble(deserializeField[2])); break; }
                        case "float":   {field.setFloat(deserializeObject,  Float.parseFloat(deserializeField[2])); break; }
                        case "long":    {field.setLong(deserializeObject,   Long.parseLong(deserializeField[2])); break; }
                        case "int":     {field.setInt(deserializeObject,    Integer.parseInt(deserializeField[2])); break; }
                        case "String":  {field.set(deserializeObject, deserializeField[2]);  break; }
                        default:{  // ******* object field
                            // TODO deserialization for arrays and collections
                            field.set(deserializeObject, deserialization(reader, deserializeField[1]));
                        }
                    }
            }
            newString = reader.readLine();
            while(newString.startsWith("\t"))  // ******* removing tabulation from the string
                newString = newString.substring(1, newString.length());
        }
        return deserializeObject;
    }
}
