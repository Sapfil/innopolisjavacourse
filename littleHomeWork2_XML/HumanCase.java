package ru.innopolis.uni.course2.littleHomeWork2_XML;

/**
 * some class for serialization/deserialization example
 * have primitive fields and  object fields
 *
 * current version of serialization/deserialization don't support
 * array-fields, collection fields and inner classes
 */
public class HumanCase{
    private final String game = "fallout2";
    private final int someInt = 42;
}
