package ru.innopolis.uni.course2.littleHomeWork2_XML;

import javax.xml.parsers.ParserConfigurationException;
import java.io.*;

/**
 * Example of using Serializer_Deserializer-machine.
 * 1) Creates some object, serialize it to new XML-file.
 * 2) Deserialize XML to clone object.
 * 3) Check equals of original-object and clone
 * !!! WARNING !!! created file exist in system after the end of program.
 * @author  Andrey Kostrov on 08.11.2016.
 */
public class MainExample {

    public static void main(String[] args) throws IOException, IllegalAccessException, ParserConfigurationException, InstantiationException, ClassNotFoundException {
        // ***** object for cloning
        Human human0 = new Human(100, "Andrey_Kostrov");

        // **** preparing XML-file for serialization
        File newFile = new File(human0.getClass().getSimpleName() + "Serialized.xml");
        FileWriter writer = new FileWriter(newFile);
        writer.write("<?xml version=\"1.0\"?>");
        String tabulation = "";

        // ***** SERIALIZATION
        Serializer_Deserializer.serialization(human0, null, writer, tabulation);

        // ***** closing file.
        writer.flush();
        writer.close();

        // ***** preparing XML-file for reading. Cutting first string wih XML-version
        BufferedReader reader = new BufferedReader(new FileReader(human0.getClass().getSimpleName() + "Serialized.xml"));
        reader.readLine(); // xml version - not needed

        // ***** creating stub-object for cloning
        Object humanClone = human0.getClass().newInstance();

        // ***** reading first XML-line with object-type
        reader.readLine();

        // ***** DESERIALIZATION
        humanClone = Serializer_Deserializer.deserialization(reader, humanClone.getClass().getName());

        // ***** equals-check
        if (humanClone.equals(human0)){
            System.out.println("Successfully teleportation");
        }
    }
}
