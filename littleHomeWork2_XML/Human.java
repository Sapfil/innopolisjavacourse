package ru.innopolis.uni.course2.littleHomeWork2_XML;

/**
 * some class for serialization/deserialization example
 * have primitive fields and  object fields
 *
 * current version of serialization/deserialization don't support
 * array-fields, collection fields and inner classes
 */
public class Human {

    private int money;
    private String name;
    private HumanCase myHumanCase = new HumanCase();
    private byte JavaSkill = 100;

    //private String[] friends = new String[]{"nobody" , "nobody", "nobody"};

    public Human(){this(0, "noname");}
    public Human(int money, String name) {
        this.money = money;
        this.name = name;
//        this.friends[0] = name + "'s friend0";
//        this.friends[1] = name + "'s friend1";
//        this.friends[2] = name + "'s friend2";
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Human human = (Human) o;

        if (money != human.money) return false;
        return name.equals(human.name);
    }

    @Override
    public int hashCode() {
        int result = money;
        result = 31 * result + name.hashCode();
        return result;
    }
}
