package ru.innopolis.uni.course2.lection6;

/**
 * Container for single long-primitive value *
 * @Author by Andrey Kostrov on 03.11.2016.
 */
public class ByteBox {

    private byte value;

    /**
     * standart-style Constructor
     * Creates instance containing zero-value
     */
    public ByteBox() {this((byte)0);}

    /**
     * standart-style Constructor
     * Creates instance containing input value
     * @param value input value
     */
    public ByteBox(byte value) {
        this.value = value;
    }

    /**
     * standard-style getter
     * @return long-primitive value
     */
    public byte getValue() {
        return value;
    }

    /**
     * standard-style setter
     * destroys previous container value and set input value
     * @param value absolute input value
     */
    public void setValue(byte value) {
        this.value = value;
    }

    /**
     * increases current container value by input value
     * @param value input value for increasing container value
     */
    public void addValue(byte value){
        this.value += value;
    }
}
