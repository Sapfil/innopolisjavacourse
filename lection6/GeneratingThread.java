package ru.innopolis.uni.course2.lection6;

/**
 * Thread generates random byte between 0 and 99 every 1 second
 * Container with generated number is monitor for clients of this Thread
 * Thread notify() only one client to obtain value
 *
 * Client can set value to -1 - it means that client is stopped and
 * this generating thread must be interrupted
 *
 * @author  Andrey Kostrov on 08.11.2016.
 */
public class GeneratingThread extends Thread{

    private final ByteBox container;

    GeneratingThread(ByteBox container){
        this.container = container;
    }

    @Override
    public void run(){
        while (!isInterrupted()) {

            synchronized (container){
                if (container.getValue() < 0)
                    interrupt();
            }

            synchronized (container) {
                container.setValue(this.generateNewNumber());
            }

            synchronized (container) {
                container.notify();
            }

            try {
                Thread.sleep(1_000L);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }

    /**
     * standars-style generator
     * @return random byte number between 0 and 99
     */
    private byte generateNewNumber(){
        return (byte)(Math.random()*100);
    }
}
