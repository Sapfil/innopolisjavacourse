package ru.innopolis.uni.course2.lection6;

import java.util.HashMap;
import java.util.Map;

/**
 * Thread contains  Hash map
 * Keys means number value
 * container means count - how much times number was generated
 *
 * if some number generated >=5 times - everything must be interrrupted
 *
 * Client can set monitor to -1 - it means that client is stopped and
 * this everybody must be interrupted
 *
 * @author  Andrey Kostrov on 08.11.2016.
 */
public class ContainingThread extends Thread{

    private final ByteBox container;
    private final Map<Byte, Byte> collection = new HashMap<>();

    ContainingThread(ByteBox container){
        this.container = container;
    }

    @Override
    public void run(){
        while(!isInterrupted()) {
            long startWaitingTime = System.currentTimeMillis();
            while (System.currentTimeMillis() - startWaitingTime < 5_000L) {
                try {
                    synchronized (container) {
                        container.wait();
                    }
                    synchronized (container) {
                        if (!this.addValueToCollection(container.getValue())) {
                            container.setValue((byte) -1);
                            interrupt();
                        }
                    }
                    } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
            this.outputCurrentResults();
        }
    }

    private boolean addValueToCollection(byte value){
        if (collection.get(value) != null) {
            collection.put(value, (byte) (collection.get(value) + 1));
        }
        else collection.put(value, (byte)1);
        return collection.get(value) < 5;
    }

    private void outputCurrentResults(){
        System.out.println("5-SECONDS OUTPUT");
        for (int i=0; i< 100; i++)
            if(collection.get((byte)i) != null )
                System.out.println("Number " + i + " generated " + collection.get((byte)i) + " times.");
    }
}
