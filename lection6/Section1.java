package ru.innopolis.uni.course2.lection6;

/**
 *
 * by Andrey Kostrov on 08.11.2016.
 */
public class Section1 {

    static ByteBox monitor = new ByteBox((byte)0);

    public static void main(String[] args) {

        Thread container = new ContainingThread(monitor);
        Thread generator = new GeneratingThread(monitor);
            new ContainingThread(monitor).start();
            new GeneratingThread(monitor).start();

        System.out.println("Main thread stopped");
    }
}
