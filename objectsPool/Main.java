package ru.innopolis.uni.course2.objectsPool;

/**
 * Program emulates pool and some clients.
 * Clients trying to get Objects from the pool,
 * holding them for some time,
 * trying to return holden Object to the pool
 * and waiting still for some time without object.
 *
 * If client can't get object because there are no objects in the pool right now,
 * or if client can't return object because pool is full -
 * in that two cases objects trying to wait for this opportunities.
 * If there is no opportunity to get or to return object during waiting time - client interrupts.
 * Max 'holding-object'-time, max 'standing-still'-time and 'max-waiting' time
 * are set in clients class static fields.
 * Individual time intervals are random for each class exemplar.
 *
 * Besides there are some adder-thread that trying to add new object to the pool every N seconds.
 * If adder cant add new object to the pool because pull is full -
 * adder trying to wait for this opportunity times during N seconds.
 * If there is no opportunity to add new object during waiting time - adder interrupts.
 * Max time (N) is set in adder class by static field
 *
 * Start clients count, start pool size and start objects count in the pool -
 * are set by static fields in Main-class
 *
 * @Author  Andrey Kostrov on 07.11.2016.
 */
public class Main {

    private static final int startSizeOfPool = 10;
    private static final int startObjectCountInPool = 5;
    private static final int poolClientsCount = 13;

    static ObjectPool pool = new ObjectPool(startSizeOfPool);

    public static void main(String[] args) {
        for (int i = 0; i < startObjectCountInPool; i++) {
            pool.addObject();
        }

        Client.pool = pool;
        ObjectsAdder.pool = pool;
        new ObjectsAdder().start();

        for (int i = 0; i< poolClientsCount; i++){
            new Client().start();
        }
        pool.run();
    }
}
