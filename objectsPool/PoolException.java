package ru.innopolis.uni.course2.objectsPool;

/**
 * wrapper for Standart Exception
 * @Author Andrey Kostrov on 07.11.2016.
 */
class PoolException extends Exception{
    public PoolException(String s) {
        super(s);
    }
}
