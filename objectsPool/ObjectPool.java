package ru.innopolis.uni.course2.objectsPool;

import java.util.LinkedList;

/**
 * Pool based on FIFO0idea
 * @Author Andrey Kostrov on 07.11.2016.
 */
public class ObjectPool extends LinkedList<Object> implements Runnable{

    final Object isNotFullFlag = new Object();
    final Object isNotEmptyFlag = new Object();

    private int poolSize = 0;


    /**
     * Standard-style constructor. Creates zero-sized pool.
     * Need to call setSize-method with not-null parameter for
     * creating some space in the pool
     */
    ObjectPool(){this(0);}

    /**
     * Standard-style constructor.
     * @param size start size of the pool
     */
    ObjectPool(int size){
        if (this.poolSize < size)
            this.setSize(size);
    }

    /**
     * Adding new object to the pool
     * @return TRUE if there were free space in the pool
     * and object added or FALSE if there is no free space in the pool
     */
    public synchronized boolean addObject(){

            if (this.size() < poolSize) {
                this.add(new Object());
                synchronized (isNotEmptyFlag) {
                    isNotEmptyFlag.notify();
                }
                return true;
            }
            else
                return false;

    }

    /**
     * Can add some free space to the pool
     * Input value must be > current pool max size
     * @param poolSize new pool size (can t be <= current size)
     */
    public void setSize(int poolSize){
        this.poolSize = poolSize;
    }

    /**
     * Return first object from the pool
     * @return first objcet from the pool - if there is at least one object in the pool
     * or return null if there are no objects in the pool
     */
    public synchronized Object getObject () {
        if (this.size() > 0) {
                Object object = this.getFirst();
                this.removeFirst();
                synchronized (isNotFullFlag) {
                    isNotFullFlag.notify();
                }
                return object;
        }
        else
            return  null;
    }

    /**
     * Nearly equals to the add method
     * Add uses to add new objects and return is used to return objects,
     * that have been in the pool some time earlier
     * @param object
     * @return TRUE if there were free space in the pool
     * and object returned to the pool
     * wor FALSE if there is no free space in the pool
     */
    public synchronized boolean returnObject(Object object){
        if (this.size() < poolSize) {
            this.add(object);
            synchronized (isNotEmptyFlag) {
                isNotEmptyFlag.notify();
            }
            return true;
        }
        return false;
    }

    @Override
    /**
     * logger -
     * 1) 'sout' current objects count every 0.1 sec
     * 2) 'sout' current total active threads count
     */
    public void run() {
        while(true){
            synchronized (this) {
                try {
                    this.wait(500);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
            System.out.println("Objects in pool " + this.size() + " | ThreadsCount " + Thread.activeCount());
        }
    }
}
