package ru.innopolis.uni.course2.objectsPool;

/**
 * Thread that gets object from the pool, holding it for some random time,
 * returning it to the pool and waiting still some random time.
 *
 * If this is not possible to get object from the pool or
 * it is not possible to return object to the pool - thread waits for some time.
 * If thread waits too long and there is no opportunity to get object orr return the object -
 * thread interrupts.
 *
 * Max object-holding time, max standing still time, max waiting time -
 * are set by private static final long values in milliseconds
 */
public class Client extends Thread {

    static ObjectPool pool;
    private Object myObject = null;
    private static final long MAX_WAITING_TIME = 5_000L;
    private static final long MAX_HOLDING_TIME = 10_000L;
    private static final long MAX_TIME_WITHOUT_OBJECT = 2_000L;

    @Override
    /**
     * Phase 1: Thread is trying to get Object from pool during maxWaitTime
     * If there are no available objects during maxWaitTime - thread interrupts
     *
     * Phase 2: Thread waiting for random time (max is set by static field)
     *
     * Phase 3: Thread trying to return object to the pool during maxWaitTime
     * If pool is full after maxWaiTime - tread interrupts
     *
     * Phase 4: Thread waiting for random time (maxTimeWithoutObject)
     *
     * maxWaitTime, maxHoldingTime, maxHoldingTime -
     * initiates for every class exemplar by random from zero to static constants of class
     */
    public void run() {

        long maxWaitTime            = (long)(Math.random() * MAX_WAITING_TIME);
        long maxHoldingTime         = (long)(Math.random() * MAX_HOLDING_TIME);
        long maxTimeWithoutObject   = (long)(Math.random() * MAX_TIME_WITHOUT_OBJECT);

        while (!isInterrupted()) {

            // ****** trying to get object from pool
            try {
                    while(!isInterrupted()) {
                            myObject = pool.getObject(); // calling synchronized method

                        if (myObject != null) {
                            System.out.println(this.getName() + " got new Object");
                            break;
                        }

                        else {
                            long startTime = System.currentTimeMillis();
                            synchronized (pool.isNotEmptyFlag) {
                                pool.isNotEmptyFlag.wait(maxWaitTime);
                            }
                            if (System.currentTimeMillis() - startTime > maxWaitTime)
                                throw new PoolException("Thread " + this.getName() + " can't wait longer for free Object and must be interrupted");
                        }
                    }
            } catch (PoolException e) {
                System.out.println(e.getMessage());
                    this.interrupt();
            } catch (InterruptedException e) {
                e.printStackTrace();
            }

            // ******* waiting some random time - holding object
            if (!isInterrupted()) {
                try {
                    synchronized (this) {
                        wait((long) (Math.random() * maxHoldingTime));
                    }
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }

            // ******* trying to return object to the pool
            if (!isInterrupted())
            try {
                long startTime = System.currentTimeMillis();
                boolean isObjectAdded;
                {
                    while(true) {
                        isObjectAdded = pool.returnObject(myObject); // calling synchronized method

                        if (isObjectAdded) {
                            System.out.println(this.getName() + " returned object");
                            break;
                        }

                        else {
                            synchronized (pool.isNotFullFlag) {
                                pool.isNotEmptyFlag.wait(maxWaitTime);
                            }
                            if (System.currentTimeMillis() - startTime > maxWaitTime)
                                throw new PoolException("Thread " + this.getName() + " can't wait longer for free space and must be interrupted");
                        }
                    }
                }
            } catch (PoolException e) {
                System.out.println(e.getMessage());
                this.interrupt();
            } catch (InterruptedException e) {
                e.printStackTrace();
            }

            // ******* waiting some random time without object
            if (!isInterrupted()) {
                try {
                    synchronized (this) {
                        wait((long) (Math.random() * maxTimeWithoutObject));
                    }
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        }
    }
}
