package ru.innopolis.uni.course2.objectsPool;

/**
 * Thread that adds new objects to the pool
 *
 * If this is not possible to return object to the pool
 * - thread waits for some time.
 * If thread waits too long and there is no opportunity to add new object to the pool-
 * thread interrupts.
 *
 * Max waiting time -
 * is set by private static final long value in milliseconds
 *
 * @Author Andrey Kostrov on 07.11.2016.
 */
public class ObjectsAdder extends Thread{

    static ObjectPool pool;
    private static final long WAITING_TIME_BEFORE_ADDING_NEXT_OBJECT = 10_000L;
    private static final long WAITING_TIME_TRYING_TO_ADD_NEW_OBJECT = WAITING_TIME_BEFORE_ADDING_NEXT_OBJECT;

    @Override
    /**
     * Phase 1: Thread waiting for some time (is set by static field)
     *
     * Phase 2: Thread trying to add object to the pool during maxWaitTime
     * If pool is full after maxWaitTime - tread interrupts
     * Time if phase 1 and in phase 2 is equals by value.
     *
     * So, Maximal time from adding previous Object to adding next Object
     * is about 2*maxWaitingTime (in fact it is a little bit more
     * because of time on execution some code between first and second waiting parts).
     */
    public void run(){

        //this.setDaemon(true);

        while(!isInterrupted()) {
            try{
                synchronized (this) {
                    wait(WAITING_TIME_BEFORE_ADDING_NEXT_OBJECT);
                }

                boolean isObjectAdded = false;
                long startWaitingTime = System.currentTimeMillis();
                while(!isObjectAdded) {

                    isObjectAdded = pool.addObject();

                    if (!isObjectAdded) {
                        synchronized (pool.isNotFullFlag) {
                            pool.isNotFullFlag.wait(WAITING_TIME_TRYING_TO_ADD_NEW_OBJECT);
                        }
                        if (System.currentTimeMillis() - startWaitingTime > WAITING_TIME_TRYING_TO_ADD_NEW_OBJECT)
                            throw new PoolException("Objects adder can't wait longer and must be interrupted.");
                    }
                    else
                        System.out.println("New object added to the pool.");
                }

            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            catch (PoolException e){
                this.interrupt();
                System.out.println("Pull is full, objects adder interrupted after "
                        + WAITING_TIME_TRYING_TO_ADD_NEW_OBJECT /1000 + " seconds-waiting period.");
            }
        }
    }
}
