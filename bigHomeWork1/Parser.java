package ru.innopolis.uni.course2.bigHomeWork1;

import ru.innopolis.uni.course2.bigHomeWork1.boxes.BooleanBox;
import ru.innopolis.uni.course2.bigHomeWork1.boxes.LongBox;

import java.io.*;

/**
 * Class is searching positive n%2==0 numbers, adding them to outer "total sum". *
 * @see SourceDeterminator - it creates BudderedReader for <tt>this class</tt>
 *                           from different source-types.
 * @author by Andrey Kostrov on 03.11.2016.
 */
public class Parser extends Thread{

    /**
     * BufferedReader created in
     * @see SourceDeterminator
     */
    private Reader inputReader;

    /**
     * outer static field containing sum from whole threads
     */
    private final LongBox totalSum;

    /**
     * source name - needs real-time output and for Exception messages
     */
    private String source;

    private final BooleanBox proceedingFlag;
    private final LongBox processedThreadCounter;

    /**
     * Creates a new thread and then parses it
     * @param reader    BufferedReader created in SourceDeterminator     *
     * @param totalSum  outer static field containing sum from whole threads
     * @param source    source name - needs real-time output and for Exception messages
     * @see SourceDeterminator
     */
    public Parser(Reader reader, LongBox totalSum, String source, BooleanBox proceedingFlag, LongBox processedThreadsCounter){

        this.inputReader = reader;
        this.totalSum = totalSum;
        this.source = source;
        this.proceedingFlag = proceedingFlag;
        this.processedThreadCounter = processedThreadsCounter;

    }

    /**
     * Searching positive and (n%2 ==0) numbers in char-sequence from BufferedReader
     * If such number found - calls private class for adding number value to outer total sum
     * @see .summator();
     */
    @Override
    public void run(){

        // *** (reinventing the wheel)
        // *** logic for parsing char-based input source
        char spaceChar = ' ' ,minusChar = '-', prevChar;
        char currentSymbol = spaceChar;
        boolean minusFound = false;
        long currentValue = 0L;
        String finishMessage = null;

        //TODO try-with-resource
        // try (inputReader) <-- WHY THIS NOT WORKING ???!!!

        try (BufferedReader reader = new BufferedReader(inputReader)){
            String inputString = reader.readLine();
            char[] inputCharSequence = inputString.toCharArray();
            int currentPositionInSequence = 0;

            while (currentPositionInSequence < inputCharSequence.length){
                prevChar = currentSymbol;
                currentSymbol = inputCharSequence[currentPositionInSequence];

                // *** number-char found
                if (currentSymbol >= '0' && currentSymbol <= '9') {
                    if (!minusFound && prevChar >= '0' && prevChar <= '9' || prevChar == spaceChar) {
                        currentValue *= 10;
                        currentValue += Character.getNumericValue(currentSymbol);
                    } else if (minusFound || prevChar == minusChar)
                        minusFound = true;

                    // *** space-char found
                } else if (currentSymbol == spaceChar) {
                    if (!minusFound && currentValue > 0) {
                        summator(currentValue);
                        currentValue = 0;
                    }
                    minusFound = false;
                }

                // *** minus-char found
                else if (currentSymbol == minusChar) {
                    if (prevChar != spaceChar)
                        throw new IOException("Wrong char-sequence. Minus-symbol is not after space-symbol in source " + source);
                }
                else throw new IOException("Not legal symbol is source " + source);

                // *** proceeding to next char
                currentPositionInSequence++;
            }

            // *** end of source. If last char in sequence was number-char
            if (!minusFound && currentValue > 0) {
                summator(currentValue);
            }
        } catch (IOException e) {
            synchronized (proceedingFlag) {
                proceedingFlag.setFALSE();
            }
            System.out.println("Proceeding flag is dropped down by parser that processed source " + source);
            finishMessage = e.getMessage();
        } finally{
            if (finishMessage == null) {
                if (proceedingFlag.isTRUE())
                    System.out.println("Source " + source + " -> processed successfully.");
                else
                    System.out.println("Source " + source + " -> processing stopped because global proceeding flag is down.");
            }
            else
                System.out.println("Source " + source + " -> processed unsuccessfully with error: " + finishMessage);
            synchronized (processedThreadCounter) {
                processedThreadCounter.addValue(1L);
            }
        }
    }

    /**
     * Adding inputValue to outer total sum
     * and writing some real-time info to console
     * @param inputValue value for adding to total sum
     */
    private void summator(long inputValue){
        // TODO maybe needs to be synchronized
        if (inputValue%2 == 0 && proceedingFlag.isTRUE()){
            synchronized (totalSum) {
                totalSum.addValue(inputValue);
                System.out.println("Added " + inputValue
                 + " to total sum from " + source + " | Now total sum is " + totalSum.getValue());
            }
        }
    }
}







