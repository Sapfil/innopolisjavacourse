package ru.innopolis.uni.course2.bigHomeWork1;

import ru.innopolis.uni.course2.bigHomeWork1.boxes.BooleanBox;
import ru.innopolis.uni.course2.bigHomeWork1.boxes.LongBox;

import java.io.IOException;

/**
 * Program finding positive n%2==0 numbers
 * from different sources and calculating total sum of such numbers
 * Every found valid number demonstrates to the user in real-time
 * Current total sum demonstrates to the user in real-time
 * @see .main
 * @Author by Andrey Kostrov on 03.11.2016.
 */
public class BigHomeWork1 {

    /**
     * Container for mathematical sum.
     * All starting Thread put found valid values to this container
     * in their "synchronized" sections
     * At the program end totalSum demonstrates to the user
     */
    private static LongBox totalSum = new LongBox(0L);
    private static BooleanBox proceedingFlag = new BooleanBox(true);
    private static LongBox threadsProcessed = new LongBox(0L);

    /**
     * @param args array of Strings, each element of array is some resource name.
     *             can be URL or File
     */
    public static void main(String[] args) {

        LongBox totalThreads = new LongBox(args.length);
        SourceDeterminator sourceDeterminator = new SourceDeterminator();

        // *** needs for whole-program-execution-time calculation
        long startTime = System.currentTimeMillis();

        // *** creating threads for every source
        try {
            Thread currentThread;
            for (String arg : args) {
                currentThread = new Thread(new Parser(sourceDeterminator.getBufferedReader(arg), totalSum, arg, proceedingFlag, threadsProcessed));
                currentThread.start();
            }
        }

         catch (IOException e) {
             proceedingFlag.setFALSE();
             System.out.println("Stopping program. Error caused by -> " + e.getMessage());

        } finally{
            boolean isAllThreadsStopped;

            while(true) {
                isAllThreadsStopped = (threadsProcessed.getValue() == totalThreads.getValue());
                if (!isAllThreadsStopped)
                    try {
                       Thread.sleep(100L);
                    } catch (InterruptedException e) {
                        proceedingFlag.setFALSE();
                        e.printStackTrace();
                    }
                else{
                    System.out.print("Program ended in " + (System.currentTimeMillis() - startTime) + " milliseconds -> ");
                    if (proceedingFlag.isTRUE()) {
                        System.out.println("successfully.");
                        System.out.println("Total sum is " + totalSum.getValue());
                        break;
                    }
                    else{
                        System.out.println("unsuccessfully.");
                        System.out.println("See previous messages to find out a crash reason.");
                        break;
                    }
                }
            }
        }
    }
}