package ru.innopolis.uni.course2.bigHomeWork1.boxes;

/**
 * by Andrey Kostrov on 06.11.2016.
 */
public class BooleanBox {

    private boolean value;

    public BooleanBox(){this(false);}
    public BooleanBox(boolean value){
        this.value = value;
    }
    public void setTRUE(){
        value = true;
    }
    public void setFALSE(){
        value = false;
    }
    public boolean getValue(){
        return value;
    }
    public void setValue(boolean value){
        this.value = value;
    }
    public void inverseValue(){
        value = !value;
    }
    public boolean isTRUE(){
        return  value;
    }

    public boolean isFALSE(){
        return  !value;
    }

}
