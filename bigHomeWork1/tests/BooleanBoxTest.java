package ru.innopolis.uni.course2.bigHomeWork1.tests;

import org.junit.Test;
import ru.innopolis.uni.course2.bigHomeWork1.boxes.BooleanBox;

import static org.junit.Assert.*;

/**
 * Simple tests for simple "container for primitive"-style class
 * All setters/getters checked
 * @author  Andrey Kostrov on 09.11.2016.
 */
public class BooleanBoxTest {
    @Test
    public void setTRUE() throws Exception {
        BooleanBox testBox = new BooleanBox(false);
        testBox.setTRUE();
        assertEquals("setTrue test:", (testBox.getValue()), true);
    }

    @Test
    public void setFALSE() throws Exception {
        BooleanBox testBox = new BooleanBox(true);
        testBox.setFALSE();
        assertEquals("setFalse test:", (testBox.getValue()), false);
    }

    @Test
    public void getValue() throws Exception {
        boolean randomBoolean = Math.random() < 0.5f;
        BooleanBox testBox = new BooleanBox(randomBoolean);
        assertEquals("getValue test",  (testBox.getValue()), randomBoolean);
    }

    @Test
    public void setValue() throws Exception {
        BooleanBox testBox = new BooleanBox(true);
        testBox.setValue(false);
        assertEquals("getValue test",  (testBox.getValue()), false);
    }

    @Test
    public void inverseValue() throws Exception {
        BooleanBox testBox = new BooleanBox(true);
        testBox.inverseValue();
        assertEquals("getValue test",  (testBox.getValue()), false);
    }

    @Test
    public void isTRUE() throws Exception {
        BooleanBox testBox = new BooleanBox(true);
        assertEquals("getValue test",  testBox.isTRUE(), true);
    }

    @Test
    public void isFALSE() throws Exception {
        BooleanBox testBox = new BooleanBox(false);
        assertEquals("getValue test",  testBox.isFALSE(), true);
    }
}