package ru.innopolis.uni.course2.bigHomeWork1.tests;

import org.junit.Test;
import ru.innopolis.uni.course2.bigHomeWork1.SourceDeterminator;

import java.io.*;

import static org.junit.Assert.*;

/**
 * "All-in-one"-style test.
 * Creates test file, containing some test string
 * Trying to create BufferedReader wrapping this file
 * 1'st time - like a file
 * 2'nd time - like a URL (with "file://lockalhost/"-prefix
 * Trying to read test string from a file.
 *
 * @author  Andrey Kostrov on 06.11.2016.
 */
public class SourceDeterminatorTest {
    @Test
    public void getBufferedReader() throws Exception {

        File stub = new File("");
        String systemPath = stub.getAbsolutePath();
        stub.delete();

        File testFile = new File(systemPath + "/testFile.txt");
        BufferedWriter testWriter= new BufferedWriter(new FileWriter(testFile));
        testWriter.write("some testing string");

        testWriter.flush();
        testWriter.flush();

        BufferedReader testReader= new BufferedReader(new FileReader(testFile));
        SourceDeterminator testSD = new SourceDeterminator();

        String testString = testReader.readLine();

        assertEquals("Testing 'file' source type",
                testSD.getBufferedReader(testFile.getPath()).readLine(),
                testString );

        assertEquals("Testing 'URL' source type",
                testSD.getBufferedReader("file://localhost/" + testFile.getPath()).readLine(),
                testString );

        testReader.close();
        testFile.delete();
    }
}