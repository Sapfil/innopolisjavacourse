package ru.innopolis.uni.course2.bigHomeWork1.tests;

import org.junit.Test;
import ru.innopolis.uni.course2.bigHomeWork1.boxes.LongBox;
import ru.innopolis.uni.course2.bigHomeWork1.Parser;
import ru.innopolis.uni.course2.bigHomeWork1.boxes.BooleanBox;

import java.io.*;

import static org.junit.Assert.*;

/**
 * Several tests for parser.
 * 1) Reading valid info from the file
 * 2) Checking that resource is closed after processing
 * 3.1) Reading invalid info from the file - wrong symbol-sequence
 * 3.2) Reading invalid info from the file - illegal symbol in sequence
 * 4) Trying to read closed resource (disconnect, HDD error)
 * @author Andrey Kostrov on 06.11.2016.
 */
public class ParserTest {

    /**
     * Creating file with valid information
     * Trying to read it.
     * Checking that "processed right" is true after processing valid file
     * Checking total sum, collected from file
     * @throws Exception if something goes wrong
     */
    @Test
    public void readingValidResource() throws Exception {
        System.out.println("-----TEST-----");
        System.out.println("->Testing processing valid symbol sequence in source. Sum must be == 100.");
        File validTestFile = new File("./validTestFile.txt");
        BufferedWriter testWriter = new BufferedWriter(new FileWriter(validTestFile));
        testWriter.write("22 33 44 -44 0 -1 34");
        testWriter.close();

        BufferedReader testReader = new BufferedReader(new FileReader(validTestFile));
        LongBox testLongBox = new LongBox(0L);
        BooleanBox proceedingFlag = new BooleanBox(true);

        Parser testParser = new Parser(testReader, testLongBox, validTestFile.getPath(), proceedingFlag, new LongBox(0L));
        testParser.run();

        assertEquals("Is valid sequence processed right", testLongBox.getValue(), 100L);
        assertEquals("Is proceedingFlag UP after valid processing", proceedingFlag.isTRUE(), true);

        if (!validTestFile.delete())
            throw new IOException("Something goes wrong in tests...");
    }

    /**
     * Trying to read from source after parser finished
     * Must throw IOException because in normal situation parser must close resource
     * @throws Exception if something goes wrong
     */
    @Test
    public void isResourceClosedAfterProcessing() throws Exception {
        System.out.println("-----TEST-----");
        System.out.println("->Testing closing source after processing");
        File validTestFile = new File("./validTestFile.txt");
        BufferedWriter testWriter = new BufferedWriter(new FileWriter(validTestFile));
        testWriter.write(" ");
        testWriter.close();

        BufferedReader testReader = new BufferedReader(new FileReader(validTestFile));

        Parser testParser = new Parser(testReader, new LongBox(0L), validTestFile.getPath(),  new BooleanBox(true), new LongBox(0L));
        testParser.run();

        try {
            testReader.readLine();
            fail("Expected IOException");
        } catch (IOException e) {
            assertEquals("Is resource closed", e.getMessage(), "Stream closed");
        }
        if (!validTestFile.delete())
            throw new IOException("Something goes wrong in tests...");
    }

    /**
     * Creating 2 files with invalid info and trying to parse them
     * 1'st file contains illegal symbol-sequence: minus not after space
     * 2'nd file contains illegal symbol
     * Must draw 2 exception messages to console.
     * Must drop "proceeding flag" 2 times (setting true manually between 1'st and 2'nd test-parts)
     * @throws Exception if something goes wrong
     */
    @Test
    public void readingInvalidResource() throws Exception{
        System.out.println("-----TEST-----");
        System.out.println("->Testing processing invalid symbol sequence in source.");
        System.out.println("Must return two 'unsuccessfully' messages ('wrong sequence' and 'invalid symbol')");
        File invalidTestFile1 = new File("./invalidTestFile1.txt");
        File invalidTestFile2 = new File("./invalidTestFile2.txt");
        BufferedWriter testWriter = new BufferedWriter(new FileWriter(invalidTestFile1));
        testWriter.write("0-");     // invalid combination - minus not after space
        testWriter.close();
        testWriter = new BufferedWriter(new FileWriter(invalidTestFile2));
        testWriter.write("*");      // invalid symbol
        testWriter.close();

        BufferedReader testReader = new BufferedReader(new FileReader(invalidTestFile1));
        LongBox testBox = new LongBox(0L);

        BooleanBox proceedingFlag = new BooleanBox(true);
        Parser testParser = new Parser(testReader, testBox, invalidTestFile1.getPath(), proceedingFlag, new LongBox(0L));
        testParser.run();

        assertEquals("Is proceedingFlag down when wrong symbol sequence found", proceedingFlag.isFALSE(), true);

        testReader = new BufferedReader(new FileReader(invalidTestFile2));
        proceedingFlag.setTRUE();
        testParser = new Parser(testReader, testBox, invalidTestFile2.getPath(), proceedingFlag, new LongBox(0L));
        testParser.run();

        assertEquals("Is proceedingFlag down when invalid symbol found", proceedingFlag.isFALSE(), true);

        if (!invalidTestFile1.delete() && invalidTestFile2.delete())
            throw new IOException("Something goes wrong in tests...");
    }

    /**
     * Testing situations like disconnect for URL or HDD error for file
     * Creating file, wrapping it to the reader, closing reader.
     * Then trying to start thread with closed resource.
     * Must draw exception message to console.
     * Must drop "proceeding flag" down.
     * @throws Exception if something goes wrong
     */
    @Test
    public void disconnectedSourceProcessing() throws Exception {
        System.out.println("-----TEST-----");
        System.out.println("->Testing situations like disconnect for URL or HDD error for file");
        File validTestFile = new File("./validTestFile.txt");
        BufferedWriter testWriter = new BufferedWriter(new FileWriter(validTestFile));
        testWriter.write("");
        testWriter.close();

        BooleanBox testBox = new BooleanBox(true);

        BufferedReader testReader = new BufferedReader(new FileReader(validTestFile));
        Parser testParser = new Parser(testReader, new LongBox(), validTestFile.getPath(), testBox, new LongBox(0L));
        testReader.close();
        if (!validTestFile.delete())
            throw new IOException("Something goes wrong in tests...");

        testParser.start();
        testParser.join();

        assertEquals("Is proceedingFlag down when source is disconnected", testBox.isFALSE(), true);
    }
}