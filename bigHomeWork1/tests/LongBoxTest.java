package ru.innopolis.uni.course2.bigHomeWork1.tests;

import ru.innopolis.uni.course2.bigHomeWork1.boxes.LongBox;

import static org.junit.Assert.*;

/**
 * Simple tests for simple "container for primitive"-style class
 * All setters/getters checked
 * @author  Andrey Kostrov on 05.11.2016.
 */
public class LongBoxTest {
    @org.junit.Test
    public void getValue() throws Exception {
        LongBox testBox = new LongBox(0);
        assertEquals(" ", testBox.getValue(), 0);
    }

    @org.junit.Test
    public void setValue() throws Exception {
        LongBox testBox = new LongBox(0);
        testBox.setValue(Long.MAX_VALUE);
        assertEquals("Setting MAX value ", testBox.getValue(), Long.MAX_VALUE);
        testBox.setValue(Long.MIN_VALUE);
        assertEquals("Setting MIN value ", testBox.getValue(), Long.MIN_VALUE);
        testBox.setValue(0L);
        assertEquals("Setting ZERO value ", testBox.getValue(), 0L);
    }

    @org.junit.Test
    public void addValue() throws Exception {
        LongBox testBox = new LongBox(0);
        testBox.addValue(Long.MAX_VALUE);
        assertEquals("Adding Long.MAX_Value to new LongBox(0)", testBox.getValue(), Long.MAX_VALUE );
    }

}